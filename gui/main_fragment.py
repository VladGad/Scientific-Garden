import customtkinter
from constants import Constants

class Settings_frame(customtkinter.CTkFrame):
    def __init__(self, master):
        super().__init__(master)

        self.checkbox_1 = customtkinter.CTkCheckBox(self, text="checkbox 1")
        self.checkbox_1.grid(row=0, column=0, padx=10, pady=(10, 0), sticky="w")
        self.checkbox_2 = customtkinter.CTkCheckBox(self, text="checkbox 2")
        self.checkbox_2.grid(row=1, column=0, padx=10, pady=(10, 0), sticky="w")

class  Main_frame(customtkinter.CTkFrame):
    def __init__(self, master):
        super().__init__(master)


class  Params_frame(customtkinter.CTkFrame):
    def __init__(self, master):
        super().__init__(master)


class Main_fragment_gui(customtkinter.CTk):
    def __init__(self):
        super().__init__()
        self.constants = Constants()
        self.title(self.constants.name_application)
        self.geometry("400x180")
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        self.checkbox_frame = Settings_frame(self)
        self.checkbox_frame.grid(row=0, column=0, padx=10, pady=(10, 0), sticky="nsw")

        self.button = customtkinter.CTkButton(self, text="my button", command=self.button_callback)
        self.button.grid(row=3, column=0, padx=10, pady=10, sticky="ew")

    def button_callback(self):
        print("button pressed")


app = Main_fragment_gui()
app.mainloop()
